# THistoire

Ceci est un fichier *unfichier.md* contenu dans le (sous-)dossier *undossier* du dossier princpal contenant TOUS les fichiers du site **/docs**

## Syntaxes de Markdown

### Texte

#### FORCER LE RETOUR À LA LIGNE

:warning: AJOUTER EXACTEMENT **DEUX ESPACES** EN FIN DE LIGNE

#### Italique

Ce extrait est *en italique*

#### En Gras (/ Bold)

Cet extrait est **en gras**

#### Des couleurs

Extrait de Texte en <red>rouge</red> ou <rouge>celui-ci</rouge>

Extrait de Texte en <bred>rouge ET en Gras (BOLD</bred> ou <brouge>celui-ci</brouge>

Autres Couleurs:

<blue>Blue..</blue> ou <bleu>Blue..</bleu>,  
<bblue>Bleu ET GRAS..</bblue> ou <bbleu>Bleu ET GRAS..</bbleu>,  
<green>Vert..</green> ou <vert>Vert..</vert>  
<bgreen>Vert ET GRAS..</bgreen> ou <bvert>Vert et GRAS..</bvert>  

etc.. 

#### Des Listes à Puces/ Bullets Lists

* Des Rois
* Des Pauvres
* De l'Ambition

#### Des Listes Ordonnées

1. Premier Point
1. Deuxième Point
1. Troisième Point

ou encore

1. Premier Point
2. Deuxième Point
3. Troisième Point

ou encore

4. Premier Point
7. Deuxième Point
9. Troisième Point

### Liens Cliquables

#### Liens SANS TOOLTIPS

[cliquer ici](https://eskool.gitlab.io/thistoire)

#### Liens AVEC TOOLTIPS

[[survoler ce lien](https://eskool.gitlab.io/thistoire)]{hello}

(survoler 1 seconde la souris sur le lien ci-dessus, sans cliquer)

### Images

On peut aussi ajouter des tooltips aux images... (non démontré ci-dessous)

#### Images SANS LÉGENDES

![Une Image Non Centrée, NON Retaillée, Sans Légende](../img/petite-image.png)

![Une Image Centrée, NON Retaillée, Sans Légende](../img/petite-image.png){.center}

(Une image avec la "CLASSE" `.center` est centrée.)
ATTENTION LE POINT `.` EST ICI OBLIGATOIRE !!! (contrairement aux tooltips)

![Une Image Centrée, NON Retaillée, Sans Légende](../img/petite-image.png){.center .box}

(Une image avec la "CLASSE" `.box` entoure l'image: avec un effet boîte, avec une ombre + des bords arrondis)

![Une Image Centrée, Retaillée, Sans Légende](../img/petite-image.png){.center width=40%}

![Une Image Centrée, retaillée, Sans Légende](../img/petite-image.png){.center width=100%}

#### Images AVEC LÉGENDES


<figure>
<img src="../img/petite-image.png">
<figcaption>AVEC UNE LÉGENDE</figcaption>
</figure>

### Tableaux

| Titre 1 | Titre 2 |
| :-: | :-: |
| 11 | 12 |
| 21 | 22 |

### A2

Blabla

